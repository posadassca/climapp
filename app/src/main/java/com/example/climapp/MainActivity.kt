package com.example.climapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    var tvCiudad:TextView? = null
    var tvGrados:TextView? = null
    var tvEstado:TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvCiudad = findViewById(R.id.tvCiudad)
        tvGrados = findViewById(R.id.tvGrados)
        tvEstado = findViewById(R.id.tvEstado)

        val ciudad:String = intent.getStringExtra("com.example.climapp.ciudades.CIUDAD")
        //Toast.makeText(this, ciudad, Toast.LENGTH_SHORT).show()

        if(Network.verificarRed(this)){
            /*Ejecutar solicitud HTTP
                Cód OpenWeatherMap -- > d22ad76caeca455fc55e122b59ca5a0c
                El Talar ID: 3434261
            */
            solicitudHttpVolley("http://api.openweathermap.org/data/2.5/weather?id="+ciudad+"&appid=d22ad76caeca455fc55e122b59ca5a0c&units=metric&lang=es")
        } else{
            Toast.makeText(this, "No hay conexión a Internet", Toast.LENGTH_SHORT).show()
        }
    }

    /* Método para Volley
    *           Es importante visualizar que realizar la misma petición HTTP con ésta libreria, solo depende de que creamos
    *           una función y un par de lineas de código.
    * La misma soporta multiples solicitudes HTTP.
    * */
    private fun solicitudHttpVolley(url:String){
        val queue = Volley.newRequestQueue( this )
        val solicitud = StringRequest(Request.Method.GET, url, Response.Listener<String>{
                response ->
            try {
                Log.d("solicitudHttpVolley", response)

                val gson = Gson()
                val ciudad = gson.fromJson(response, Ciudad::class.java)
                tvCiudad?.text = ciudad.name
                tvGrados?.text = ciudad.main?.temp.toString() + "°"
                tvEstado?.text = ciudad.weather?.get(0)?.description

            } catch (e: Exception){

            }
        }, Response.ErrorListener { })

        queue.add(solicitud)
    }
}
