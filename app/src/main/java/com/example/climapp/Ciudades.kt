package com.example.climapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast

class Ciudades : AppCompatActivity() {
    val TAG:String ="com.example.climapp.ciudades.CIUDAD"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ciudades)

        val btMexico = findViewById<Button>(R.id.btMexico)
        val btBerlin = findViewById<Button>(R.id.btBerlin)
        val btShanghai = findViewById<Button>(R.id.btShanghai)

        btMexico.setOnClickListener(View.OnClickListener {
            /* Creación de intent para la comunicación entre componentes y el envío de información a través del intent*/
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra(TAG, "3434261")
            startActivity(intent)
        })

        btBerlin.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra(TAG, "2950159")
            startActivity(intent)
        })

        btShanghai.setOnClickListener(){
            val intent = Intent (this, MainActivity::class.java)
            intent.putExtra(TAG, "1796236")
            startActivity(intent)
        }
    }
}
